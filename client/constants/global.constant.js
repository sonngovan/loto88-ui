export const authenticationConstants = {
    AUTHENTICATED: 'AUTHENTICATED',
    UNAUTHENTICATED: 'UNAUTHENTICATED',
    IDLE_TIME: 300000
}

export const DEFAULT_PASSWORD = 'user123';

export const AUTHENTICATION = 'authManagement';

export const USERS = 'users';

export const COINS = 'coins';

export const WALLETS = 'user-wallets';

export const COIN_LIST = 'coin-list';

export const COIN_INFO = 'coin-info';
export const REGISTER_MESSAGE = {
	CODE_SENT_WARNING: 'Verification code will expire in 15 minutes!',
	CODE_SENT_ERROR: 'There was some error!',
	RESET_PASSWORD_SUCCESS: 'You have been reset your password, login to get more infomation!'
};
import { message } from 'antd';

const Message = ({ type, messageText }) => {
    return (
        message[type](messageText)
    )
}

export default Message;

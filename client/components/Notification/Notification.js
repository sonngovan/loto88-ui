import { notification } from 'antd';

const Notification = ({ title, messageText }) => {
	return (
		notification.open({
			message: title,
			description: messageText,
		})
	);
};

export default Notification;
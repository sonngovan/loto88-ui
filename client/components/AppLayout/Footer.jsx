import React from 'react';
import { Layout } from 'antd';
import classnames from 'classnames'
import 'antd/dist/antd.less';
import './header.less'

const Footer = () => {
  return (
    <Layout.Footer style={{ textAlign: 'center', position: "sticky", bottom: "0" }}>Loto88 ©2020 Created by DsCo</Layout.Footer>
  );
}

export default Footer;
import React, { Fragment, Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Trans, withI18n } from '@lingui/react'
import { Layout, Menu, Dropdown, Avatar, Divider, Spin, Button, Icon } from 'antd';
import { logoPath } from '../../config';
import 'antd/dist/antd.less';
import './header.less'
@inject(stores => stores)
@withI18n()
@observer
class Header extends Component {
  constructor(props) {
    super(props);
  }

  handleChange = (data) => {
    const { setLocalStorage } = this.props.globalStore;
    setLocalStorage(data);
  }
  
  onClick({ key }) {
  console.log('key: ', key);
    if (key === 'signout') {
      this.props.userStore.logout();
    }
  }

  render() {
    const { language, languages } = this.props.globalStore;
    const { authenticationStatus, user, logout } = this.props.userStore;
    const leftContent = [
      <Fragment>
        <div>
            <Link to="/"><Avatar size="large" className="logo" src={'http://cdn.onlinewebfonts.com/svg/img_364776.png'}/></Link>
            <Divider type="vertical" className="divider"/>
        </div>
      </Fragment>
    ]
    const userDropdown = (
      <Menu>
        <Menu.Item>
        <Icon type="solution" /> Tài khoản
        </Menu.Item>
        <Menu.Item>
        <Icon type="dollar" /> Nạp tiền
        </Menu.Item>
        <Menu.Item onClick={logout}>
          <Icon type="logout" /> Đăng xuất
        </Menu.Item>
      </Menu>
    );
    const rightContent = authenticationStatus === undefined ? [
      <Spin></Spin>
    ] : (authenticationStatus !== 'UNAUTHENTICATED' ? 
    [
      <Dropdown overlay={userDropdown} placement="bottomLeft">
        <Avatar style={{ marginLeft: 8 }} src={'https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png'} />
      </Dropdown>
    ] : 
    [
      <div>
        <Link to="/user/login"><Button className="login-inline" type="primary">Đăng nhập</Button></Link>
        <Link to="/user/register"><Button>Đăng ký</Button></Link>
      </div>
    ])

    const menuLanguageDropdown = (
      <Menu
        onClick={data => {
          this.handleChange({ key: 'language', value: data.key }) 
        }}
      >
        {languages.map(item => (
          <Menu.Item key={item.languageKey}>
            <Avatar
              size="small"
              style={{ marginRight: 8 }}
              src={item.flag}
            />
            {item.title}
          </Menu.Item>
        ))}
      </Menu>
    )

    return (
        <Layout.Header className="header" id="layoutHeader" >
            <div className="leftContainer">{leftContent}</div>
            <div className="rightContainer">{rightContent}</div>
        </Layout.Header>
    );
  }
}

export default Header;
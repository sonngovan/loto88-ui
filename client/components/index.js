import Header from './AppLayout/Header';
import Footer from './AppLayout/Footer'
import WrapperRoute from './WrapperRoute/WrapperRoute';
import Loading from './Loading/Loading';
// import CardIndex from './CardIndex/CardIndex';
import Message from './Message/Message';
import Notification from './Notification/Notification';

export {
	Header,
	Footer,
	WrapperRoute,
	Loading,
	// CardIndex,
	Message,
	Notification,
};
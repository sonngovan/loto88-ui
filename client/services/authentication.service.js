import axios from 'axios';
import { config } from '../../config'

const loginLocalStrategy = (credenticals) => {
  return axios.post(`${config.serverUrl}/auth/login`, credenticals)
}

const loginPassportStrategy = (token) => {
console.log('token: ', token);
  return axios.get(`${config.serverUrl}/auth/re-authenticate`, {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
}


const register = async (data) => {
  return axios.post(`${config.serverUrl}/auth/register`, data)
}

export default {
  register,
  loginLocalStrategy,
  loginPassportStrategy,
}
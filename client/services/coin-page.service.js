import CollectionRestService from './collection-rest.service';
import { COIN_INFO, COINS } from '../constants/services.constant';

export const coinPageService = new CollectionRestService({ service: COINS });
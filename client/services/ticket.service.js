import axios from 'axios';
import { config } from '../../config'

const createTicket = async (data) => {
  return axios.post(`${config.serverUrl}/ticket`, data)
}

export default {
  createTicket,
}
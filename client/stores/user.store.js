import { observable, action } from 'mobx';
import authenticationService from '../services/authentication.service';
import { authenticationConstants } from '../constants/global.constant';
const { AUTHENTICATED, UNAUTHENTICATED } = authenticationConstants;

class UserStoreInit {
  constructor() {
    this.authenticationService = authenticationService;
    this.reAuthenticate()
  }
  @observable authenticationStatus;

  @observable user;

  @observable loginError;

  @action reAuthenticate = () => {
    const token = localStorage.getItem('token')
    if (token) {
      this.authenticationService.loginPassportStrategy(token)
        .then((result) => {
          this.authenticationStatus = AUTHENTICATED;
        })
        .catch((err) => {
          this.authenticationStatus = UNAUTHENTICATED;
          return;
        });
    } else {
      this.authenticationStatus = UNAUTHENTICATED;
    }
  };

  @action login = (data) => {
    return this.authenticationService.loginLocalStrategy(data)
      .then(result => {
        localStorage.setItem('token', result.data.accessToken)
        this.authenticationStatus = AUTHENTICATED;
      }).catch((err) => {
        this.loginError = '';
      });
  }
  
  @action logout = () => {
    this.user = undefined;
    this.authenticationStatus = UNAUTHENTICATED;
    localStorage.clear();
  }

  @action setProperties = (newValue) => {
    Object.assign(this, newValue);
  }
}

const UserStore = new UserStoreInit();

export default UserStore;

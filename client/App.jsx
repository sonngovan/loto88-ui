import React from 'react';
import { Redirect, Route, Switch, BrowserRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import Layout from './layouts/PrimaryLayout';
import {
  login,
  register,
  home,
  resetPassword,
} from './endpoints';
import './app.less';
import { authenticationConstants } from './constants/global.constant'
@inject(stores => stores)
@observer
class App extends React.Component {
  renderNonUser() {
    return (
      <BrowserRouter>
        <Switch>
          <Layout>
            <Route exact path={register.path} component={register.component}  />
            <Route exact path={login.path} component={login.component} />
            <Route exact path={resetPassword.path} component={resetPassword.component} />
            <Route exact path={home.path} component={home.component} />
            <Redirect to={login.path} />
          </Layout>
        </Switch>
      </BrowserRouter>
    );
  }
  renderUser() {
  console.log('renderUser: ');
    return (
      <BrowserRouter>
        <Switch>
          <Layout>
            <Route exact path={home.path} component={home.component} />
            <Redirect to={home.path} />
          </Layout>
        </Switch>
      </BrowserRouter>
    );
  }
  renderAdmin() {
    return (
      <BrowserRouter>
        <Switch>
          <Layout>
            <Route exact path={home.path} component={home.component} />
            <Redirect to={home.path} />
          </Layout>
        </Switch>
      </BrowserRouter>
    );
  }

  renderSpinner() {
    return (
      <BrowserRouter>
        <Switch>
          <Layout>
            <p>Spinner</p>
          </Layout>
        </Switch>
      </BrowserRouter>
    );
  }

  render() {
    const { authenticationStatus, user } = this.props.userStore;
    console.log('authenticationStatus: ', authenticationStatus);
    const role = user ? user.role : 'non-user';
    if (!authenticationStatus) {
      return this.renderSpinner();
    }
    if (authenticationStatus === authenticationConstants.UNAUTHENTICATED) {
      return this.renderNonUser();
    }

    return this.renderUser();
  }
}

export default App;
import React, { Component, Fragment } from 'react';
import { BackTop, Layout, Drawer, ConfigProvider } from 'antd';
import { I18nProvider } from '@lingui/react';
import { Header, Footer } from '../components';
import { observer, inject } from 'mobx-react';
import en_US from 'antd/lib/locale-provider/en_US';
import vi_VN from 'antd/lib/locale-provider/vi_VN';

const languages = {
  en: en_US,
  vi: vi_VN
}
import './PrimaryLayout.less'

const { Content } = Layout

@inject(stores => stores)
@observer
class PrimaryLayout extends Component {
  render() {
    const { globalStore, userStore } = this.props;
    const { children } = this.props;
    const { language, catalogs } = globalStore;
    return (
      <ConfigProvider locale={languages[language]}>
        <I18nProvider language={language} catalogs={catalogs}>
        <div className="layout">
          <Layout>
              <Header />
              <Content
                className="main-content"
              >
                  {children}
              </Content>
              <Footer />
              <BackTop />
          </Layout>
        </div>
        </I18nProvider>
      </ConfigProvider>
    )
  }
}

export default PrimaryLayout

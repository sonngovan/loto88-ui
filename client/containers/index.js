import Login from './Login/Login';
import Register from './Register/Register';
import HomePage from './HomePage/HomePage';
import ResetPassword from './ResetPassword/ResetPassword';

export {
    Login,
    Register,
    HomePage,
    ResetPassword,
}
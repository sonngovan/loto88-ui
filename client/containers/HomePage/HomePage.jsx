import React, { Fragment, Children } from 'react';
import { observer } from 'mobx-react';
import { Menu } from 'antd'
import './home-page.less'
import HomePageStore from './home-page.store';
import { Xsmb } from '../Xsmb/Xsmb.component';
@observer
class HomePage extends React.Component {
    constructor(props) {
        super(props)
        this.store = HomePageStore;
    }

    onclick({ children }) {
      console.log('children: ', children);
      this.store.setProperties({ children });
    }
    render() {
      const { children, setProperties } = this.store;
      console.log('children: ', children);
        return (
          <Fragment>
            <Menu mode="horizontal" onClick={setProperties}>
              <Menu.SubMenu
                title={
                  <span className="submenu-title-wrapper">
                    Siêu tốc
                  </span>
                }
              >
                <Menu.Item>1 Giây</Menu.Item>
                <Menu.Item>45 Giây</Menu.Item>
                <Menu.Item>1 Phút</Menu.Item>
                <Menu.Item>1,5 Phút</Menu.Item>
              </Menu.SubMenu>

              <Menu.SubMenu
                title={
                  <span className="submenu-title-wrapper">
                    Miền Nam
                  </span>
                }
              >
                <Menu.Item>Option 1</Menu.Item>
                <Menu.Item>Option 2</Menu.Item>
                <Menu.Item>Option 3</Menu.Item>
                <Menu.Item>Option 4</Menu.Item>
              </Menu.SubMenu>

              <Menu.SubMenu
                title={
                  <span className="submenu-title-wrapper">
                    Miền Trung
                  </span>
                }
              >
                <Menu.Item>Option 1</Menu.Item>
                <Menu.Item>Option 2</Menu.Item>
                <Menu.Item>Option 3</Menu.Item>
                <Menu.Item>Option 4</Menu.Item>
              </Menu.SubMenu>

              <Menu.SubMenu
                title={
                  <span className="submenu-title-wrapper">
                    Miền Bắc
                  </span>
                }
              >
                <Menu.Item key="xsmb">Xổ số miền bắc</Menu.Item>
                <Menu.Item key="xsmbdb">Đặc biệt 18h25</Menu.Item>
              </Menu.SubMenu>
            </Menu>
            {
              children === 'xsmb' ? <Xsmb />
              : <div>a</div>
            }
          </Fragment>
        )
    }
}

export default HomePage;
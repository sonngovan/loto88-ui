import { observable, action } from 'mobx';

class HomePageStoreInit {
  constructor() {}

  @observable children = 'xsmb';

  @action setProperties = (newValue) => {
    Object.assign(this, { children: newValue.key });
  }
}

const HomePageStore = new HomePageStoreInit();

export default HomePageStore;
import React from 'react';
import { observer } from 'mobx-react';
import 'antd/dist/antd.less';
import { Trans, withI18n } from '@lingui/react';
import { Link } from 'react-router-dom';
import {
  Form,
  Input,
  Icon,
  Checkbox,
  Button,
  Result
} from 'antd';
import RegisterStore from './register.store';
import './register.less';

@withI18n()
@Form.create()
@observer
class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.store = RegisterStore;
  }
  
  handleRegister = e => {
    const { register, setProperties } = this.store;
    const { validateFieldsAndScroll } = this.props.form;
    e.preventDefault();
    setProperties({ registerStatus: 'loading' });
    setTimeout(() => {
      validateFieldsAndScroll((err, values) => {
        if (!err) {
          const { agreement, email, password, phoneNumber } = values;
          if (agreement && agreement === true) {
            register({ email, password, phoneNumber });
          } else {
            setProperties({ registerStatus: 'error' });
            return setProperties({
              message: {
                status: 'error',
                message: 'Please read & accept agreement!',
              }
            });
          }
          return setProperties({ registerStatus: 'error' });
        }
        return setProperties({ registerStatus: 'error' });
      });
    }, 1000);
  };

  refreshForm = () => {
    this.store.setProperties({
      registerStatus: undefined,
      registerError: {},
      registerStep: 'inputData',
    });
  }

  render() {
    const { i18n, form: { getFieldDecorator } } = this.props;
    const { registerStep, registerError, codeSent, registerStatus } = this.store;
    console.log('registerError: ', registerError);
    switch (registerStep) {
    case 'inputData': {
      return (
        <Form
          className="register"
        >
          {registerError && <div className="error">{registerError}</div>}
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: <Trans>The input is not valid Email!</Trans>
                },
                {
                  required: true,
                  message: <Trans>Please input your Email!</Trans>
                },
              ],
            })(
              <Input
                disabled={codeSent}
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder='Email của bạn'
              />
            )}
          </Form.Item>

          <Form.Item>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: <Trans>Hãy nhập mật khẩu!</Trans>
                },
              ],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder='Mật khẩu'
                type='password'
              />
            )}
          </Form.Item>

          <Form.Item>
            {getFieldDecorator('phoneNumber', {
              rules: [
                {
                  required: true,
                  message: <Trans>Please input your phone number!</Trans>
                },
              ],
            })(
              <Input
                prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder='Số điện thoại'
              />
            )}
          </Form.Item>

          <Form.Item>
            {getFieldDecorator('agreement', {
              valuePropName: 'checked',
              rules: [
                { required: true, message: <Trans>Please read accept agreement</Trans> },
              ]
            })(
              <Checkbox>
                <Trans>Tôi đồng ý với <Link to="/user/agreement">điều khoản!</Link></Trans>
              </Checkbox>,
            )}
            <Button
              onClick={this.handleRegister}
              type="primary"
              htmlType="submit"
              className="submit-form-button"
              loading={registerStatus === 'loading'}
            >
              <Trans>Đăng ký</Trans>
            </Button>
            <Trans>Đã có tài khoảnt? <button className="sendcode-button" onClick={this.refreshForm}><Link to="/user/login" >đăng nhập ngay!</Link></button></Trans>
          </Form.Item>
        </Form>
      );
    }

    case 'success': {
      return (
        <Result
          status="success"
          title="Registration success!"
          subTitle="Please login for more infomation!"
          extra={[
            <Button type="primary" key="login" onClick={this.refreshForm}>
              <Link to="/user/login">Login</Link>
            </Button>,
            <Button key="home" onClick={this.refreshForm}>
              <Link to="/">Home Page</Link>
            </Button>
          ]}
        />
      );
    }
    }
    
  }
}

export default RegisterForm;
          
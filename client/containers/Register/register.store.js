import { observable, action } from 'mobx';
import { userService } from '../../services/user.service';
import authenticationService from '../../services/authentication.service';
import { DEFAULT_PASSWORD } from '../../constants/global.constant';
import { Message, Notification } from '../../components';
import { REGISTER_MESSAGE } from '../../constants/message.constant';
import axios from 'axios';
import { config } from '../../../config'
class RegisterStore {
  constructor() {
    this.userService = userService;
    this.authenticationService = authenticationService;
  }

  @observable registerStep = 'inputData';

  @observable registerStatus;

  @observable codeSent = false;

  @observable registerError;

  @observable currentEmail;

  @action register = (data) => {
    this.authenticationService.register(data)
      .then(data => {
        if (data.status === 204) {
          this.setProperties({
            registerStep: 'success',
          });
        }
      })
      .catch(err => {
        const mes = err.response.data.message;
        this.setProperties({ registerError: Object.values(mes[0])[0] });
      })
  }

  // @action sendVerificationCode = (email) => {
  //   this.userService.create({
  //     email,
  //     password: DEFAULT_PASSWORD,
  //   })
  //     .then(() => {
  //       Message({
  //         type: 'warning',
  //         messageText: REGISTER_MESSAGE.CODE_SENT_WARNING,
  //       });
  //       this.setProperties({
  //         currentEmail: email,
  //         codeSent: true,
  //       });
  //     })
  //     .catch(() => {
  //       this.resendVerificationCode(email)
  //         .then(() => {
  //           this.setProperties({
  //             currentEmail: email,
  //             codeSent: true,
  //           });
  //         })
  //         .catch((registerError) => {
  //           Message({
  //             type: 'error',
  //             messageText: REGISTER_MESSAGE.CODE_SENT_ERROR,
  //           });
  //           this.setProperties({
  //             registerError,
  //             registerStatus: 'error',
  //           });
  //         });
  //     });
  // }

  // @action resendVerificationCode = (email) => {
  //   return this.authenticationService.create({
  //     action: 'resendVerifySignup',
  //     value: {
  //       email,
  //     }
  //   });
  // }

  // @action changePassword = (password) => {
  //   this.authenticationService.create({
  //     action: 'passwordChange',
  //     value: {
  //       user: { email: this.currentEmail },
  //       oldPassword: DEFAULT_PASSWORD,
  //       password,
  //     }
  //   })
  //     .then(() => {
  //       Message({
  //         type: 'success',
  //         messageText: REGISTER_MESSAGE.RESET_PASSWORD_SUCCESS,
  //       });
  //       this.setProperties({ registerStep: 'success' });
  //     })
  //     .catch((registerError) => {
  //       Message({
  //         type: 'error',
  //         messageText: REGISTER_MESSAGE.CODE_SENT_ERROR,
  //       });
  //       this.setProperties({ registerError });
  //     });
  // }

  @action setProperties = (newValue) => {
    Object.assign(this, newValue);
  }
}

const AppRegisterStore = new RegisterStore();

export default AppRegisterStore;
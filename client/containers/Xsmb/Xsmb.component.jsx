import React, { Fragment } from 'react';
import {
  Table, Row, Col, Card, Icon, Avatar,
  Select, Button, Radio, Tabs, DatePicker,
  InputNumber, Tag, message,
} from 'antd';
import { observer } from 'mobx-react';
import moment from 'moment';
import XsmbStore, { TABS } from './xsmb.store';
import './xsmb.less';
import { CountdownCard } from './CountdownCard.component';

const columnsResult = [
  {
    title: 'Giải',
    dataIndex: 'title',
    render: text => <a>{text}</a>,
  },
  {
    title: 'Kết quả',
    className: 'column-money',
    dataIndex: 'data',
    align: 'center',
    render: data => <div>{data.join(', ')}</div>,
  },
];

const columnsTicket = [
  {
    title: 'Thể loại',
    dataIndex: 'type',
    align: 'center',
  },
  {
    title: 'Số cược',
    dataIndex: 'number',
    align: 'center',
  },
  {
    title: 'Số lượng',
    dataIndex: 'count',
    align: 'center',
  },
  {
    title: 'Cấp số nhân',
    dataIndex: 'multiple',
    align: 'center',
  },
  {
    title: 'Tiền cược',
    dataIndex: 'money',
    align: 'center',
  },
  {
    title: 'Tiền thắng / 1 lần',
    dataIndex: 'ratio',
    align: 'center',
  },
];

@observer
export class Xsmb extends React.Component {
  constructor() {
    super()
    this.store = XsmbStore;
  }

  onChange = (index) => {
    this.store.fetchData(index);
  }

  handleOnChooseTen = (a) => {
    this.store.changeTenArrayStatus(a)
  }

  handleOnChooseOne = (a) => {
    this.store.changeOneArrayStatus(a)
  }

  handleRadioOneChoose = e => {
    this.store.chooseOneRadioGroup(e.target.value)
  }

  handleRadioTenChoose = e => {
    this.store.chooseTenRadioGroup(e.target.value)
  }

  handleAddTicket = () => {
    if (this.store.contract.length === 0) {
      message.error('Hãy chọn ít nhất 01 số');
    } else {
      this.store.addTicket();
    }
  }

  handleAddMultiple = (value) => this.store.changeNumberOfMultiple(value);

  render() {
    const { data, isBefore, tenArray, oneArray, ticket, changeTab } = this.store;
    console.log('isBefore: ', isBefore);
    if (data) {
      return (
        <Fragment>
        <Row gutter={16}>
          <Col className="gutter-row" span={12}>
            <Table
              id="table-result"
              columns={columnsResult}
              dataSource={data}
              bordered
              pagination={false}
              title={() => {
                let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                return (
                  <div style={{ marginBottom: 16 }}>
                    Ngày：
                    <Select
                      defaultValue={0}
                      onChange={this.onChange}
                      dropdownMatchSelectWidth={false}
                    >
                      {array.map((_, index) => <Select.Option value={index}>
                        {moment().add(isBefore? -index-2 : -index-1, 'days').format('DD-MM-YYYY')}
                      </Select.Option>)}
                    </Select>
                    <DatePicker />
                  </div>
                )
              }}
            />
          </Col>
          <Col className="gutter-row" span={12}>
            <Row>
              <Col span={12}>
                <CountdownCard />
              </Col>
              <Col className="gutter-row" span={12}>
                <Card
                  actions={[
                    <Icon type="setting" key="setting" />,
                    <Icon type="edit" key="edit" />,
                    <Icon type="ellipsis" key="ellipsis" />,
                  ]}
                >
                  <Card.Meta
                    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                    title="Card title"
                    description="This is the description"
                  />
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col className="gutter-row" span={24}>
            <Card
              id="action-card"
            >
              <Tabs onChange={changeTab} type="card">
                <Tabs.TabPane tab="Bao Lô" key={TABS.MIEN_BAC}>
                  <div className="group-type-action">
                    <div>
                      <Radio.Group defaultValue="2end" buttonStyle="default">
                        <Radio.Button value="2end">Lô 2 Số</Radio.Button>
                        <Radio.Button value="2start" disabled>Lô 2 Số Đầu</Radio.Button>
                        <Radio.Button value="3" disabled>Lô 3 Số</Radio.Button>
                        <Radio.Button value="4" disabled>Lô 4 Số</Radio.Button>
                      </Radio.Group>
                    </div>
                    <div>
                      <Radio.Group buttonStyle="default">
                        <Radio.Button value="a" disabled>Chọn Số</Radio.Button>
                        <Radio.Button value="b" disabled>Chọn Số Nhanh</Radio.Button>
                        <Radio.Button value="c" disabled>Nhập Số</Radio.Button>
                      </Radio.Group>
                    </div>
                  </div>
                  <Card.Meta
                    title={
                      <div>
                        <div className="group-choose-ten-number">
                          <div>
                            {tenArray.map(item => {
                              return <Button
                                size="large"
                                className="chooseNumber"
                                type={item.status ? "danger" : "default"}
                                shape="circle"
                                onClick={() => this.handleOnChooseTen(item.index)}
                                >{item.index}</Button>
                            })}
                          </div>
                          <div>
                            <Radio.Group size="medium" onChange={this.handleRadioTenChoose}>
                              <Radio.Button value="all">Tất cả</Radio.Button>
                              <Radio.Button value="small">Tài</Radio.Button>
                              <Radio.Button value="large">Xỉu</Radio.Button>
                              <Radio.Button value="odd">Lẻ</Radio.Button>
                              <Radio.Button value="even">Chẵn</Radio.Button>
                              <Radio.Button value="clear">Xoá</Radio.Button>
                            </Radio.Group>
                          </div>
                        </div>
                        <div className="group-choose-one-number">
                          <div>
                            {oneArray.map(item => {
                              return <Button
                                size="large"
                                className="chooseNumber"
                                type={item.status ? "danger" : "default"}
                                shape="circle"
                                onClick={() => this.handleOnChooseOne(item.index)}
                                >{item.index}</Button>
                            })}
                          </div>
                          <div>
                            <Radio.Group size="medium" onChange={this.handleRadioOneChoose}>
                              <Radio.Button value="all">Tất cả</Radio.Button>
                              <Radio.Button value="small">Tài</Radio.Button>
                              <Radio.Button value="large">Xỉu</Radio.Button>
                              <Radio.Button value="odd">Lẻ</Radio.Button>
                              <Radio.Button value="even">Chẵn</Radio.Button>
                              <Radio.Button value="clear">Xoá</Radio.Button>
                            </Radio.Group>
                          </div>
                        </div>
                        <div className="group-action">
                          <div>
                            <Tag color="volcano">
                              1 Ăn 98.5
                            </Tag>
                            <Tag color="orange">
                              Đánh hai chữ số cuối trong các giải
                            </Tag>
                          </div>
                          <div className="group-choose-ticket">
                            <InputNumber min={1} max={100} defaultValue={1} onChange={this.handleAddMultiple} size="large"/>
                            <Button size="large" onClick={this.handleAddTicket}>
                              <Icon type="plus" />Thêm Vé Cược
                            </Button>
                            <Button size="large" type="primary">
                              <Icon type="rise" />Đặt Cược
                            </Button>
                          </div>
                        </div>
                        <div className="tableTicket">
                        <Table
                          id="table-result"
                          columns={columnsTicket}
                          dataSource={ticket}
                          bordered
                          pagination={false}
                        />
                        </div>
                      </div>
                    }
                  />
                  </Tabs.TabPane>
                  <Tabs.TabPane tab="Lô Xiên" key="2">
                    Content of Tab Pane 2
                  </Tabs.TabPane>
                  <Tabs.TabPane tab="Đánh đề" key="3">
                    Content of Tab Pane 3
                  </Tabs.TabPane>
                  <Tabs.TabPane tab="Đầu đuôi" key="4">
                    Content of Tab Pane 3
                  </Tabs.TabPane>
                  <Tabs.TabPane tab="3 càng" key="5">
                    Content of Tab Pane 3
                  </Tabs.TabPane>
                  <Tabs.TabPane tab="4 càng" key="6">
                    Content of Tab Pane 3
                  </Tabs.TabPane>
                  <Tabs.TabPane tab="Lô trượt" key="7">
                    Content of Tab Pane 3
                  </Tabs.TabPane>
                </Tabs>
            </Card>
          </Col>
        </Row>
        </Fragment>
      )
    } else {
      return <div></div>
    }
  }
}
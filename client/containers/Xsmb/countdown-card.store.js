import { observable, action } from 'mobx';
import { config } from '../../../config'
import moment from 'moment';

class CountDownCardStoreInit {
  @observable durationHours;
  @observable durationMinutes;
  @observable durationSeconds;
  @observable currentDay;
  @observable isBefore = true;

  constructor() {
    this.calculateCurrentDay()
    setInterval(() => this.calculateCurrentDay(), 1000);
  }

  @action
  setProperties(newValue) {
    Object.assign(this, newValue);
  }

  @action
  calculateCurrentDay() {
    const endTime = moment(config.mien_bac_mo_thuong, 'HH:mm a');
    const endTimeTomorrow = moment(config.mien_bac_mo_thuong, 'HH:mm a').add(1, 'days');
    if(moment().isBefore(endTime)) {
      this.currentDay = moment().format('DD-MM-YYYY');
      this.dayRenderResult = moment().add(-1, 'days').format('DD-MM-YYYY');
      let duration = moment.duration(endTime.diff(moment())).asMilliseconds();
      let a = moment.utc(duration).format("HH:mm:ss");
      this.durationHours = a.slice(0,2);
      this.durationMinutes = a.slice(3,5);
      this.durationSeconds = a.slice(6,8);
    } else {
      this.isBefore = false;
      this.currentDay = moment().add(1, 'days').format('DD-MM-YYYY');
      this.dayRenderResult = moment().format('DD-MM-YYYY');
      let duration = moment.duration(endTimeTomorrow.diff(moment())).asMilliseconds();
      let a = moment.utc(duration).format("HH:mm:ss");
      this.durationHours = a.slice(0,2);
      this.durationMinutes = a.slice(3,5);
      this.durationSeconds = a.slice(6,8);
    }
  }
}

const CountDownCardStore = new CountDownCardStoreInit();

export default CountDownCardStore;
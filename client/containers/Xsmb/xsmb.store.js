import { observable, action, computed } from 'mobx';
import axios from 'axios';
import moment from 'moment';
import { config } from '../../../config';
import CountdownCardStore from './countdown-card.store';

export const TABS = {
  MIEN_BAC: 'mienbac'
}

export const TICKET_PRICE = {
  MB_LO_2_SO: 27000,
}

export const GAME_TYPE = {
  MB_LO_2_SO: 'MB_LO_2_SO',
}
class XsmbStoreInit {
  @observable data = [];
  @observable index;
  @observable selection = [];
  @observable tenArray = [];
  @observable oneArray = [];
  @observable radioGroup;
  @observable contract = [];
  @observable tabIndex = TABS.MIEN_BAC;
  @observable numberOfMultiple;
  @observable ticket = [];
  @observable gameType = GAME_TYPE.MB_LO_2_SO;

  constructor() {
  console.log('constructor: ');
    this.fetchData();
    for (let i = 0; i < 10; i += 1) {
      this.tenArray.push({
        index: i,
        status: false,
      })
      this.oneArray.push({
        index: i,
        status: false,
      })
    }
  }

  @action
  addTicket() {
    let ten = this.tenArray.filter(item => item.status).map(item => item.index).join(',')
    let one = this.oneArray.filter(item => item.status).map(item => item.index).join(',')
    console.log('this.numberOfMultiple: ', this.numberOfMultiple);
    this.ticket.push({
      type: this.gameType,
      number: `${ten} | ${one}`,
      count: this.contract.length,
      multiple: this.numberOfMultiple,
      money: this.contract.length*TICKET_PRICE[this.gameType]*this.numberOfMultiple,
      ratio: 99.5
    })
    this.chooseTenRadioGroup('clear');
    this.chooseOneRadioGroup('clear');
  }

  @action
  changeNumberOfMultiple(value) {
  console.log('value: ', value);
    this.numberOfMultiple = value;
  }

  @action
  changeTab(tab) {
    this.tabIndex = tab;
  }

  @action
  changeTenArrayStatus(number) {
    this.tenArray[number].status = !this.tenArray[number].status;
    this.calculateContract();
  }

  @action
  changeOneArrayStatus(number) {
    this.oneArray[number].status = !this.oneArray[number].status;
    this.calculateContract();
  }

  @action
  calculateContract() {
  console.log('calculateContract: ');
    this.contract = [];
    this.tenArray.map(itemTen => {
      if(itemTen.status) {
        this.oneArray.map(itemOne => {
          if(itemOne.status) {
            this.contract.push(`${itemTen.index}${itemOne.index}`)
          }
        })
      }
    })
  }

  @action
  chooseTenRadioGroup(value) {
    switch(value) {
      case 'all': {
        this.tenArray.map(item => {
          item.status = true;
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'small': {
        this.tenArray.map(item => {
          if(item.index<5) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'large': {
        this.tenArray.map(item => {
          if(item.index>4) {
            item.status = true;
          } else {
            item.status = false;
          }

          return item;
        })
        this.calculateContract();
        break;
      }
      case 'even': {
        this.tenArray.map(item => {
          if(item.index % 2 === 0) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'odd': {
        this.tenArray.map(item => {
          if(item.index % 2 !== 0) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'clear': {
        this.tenArray.map(item => {
          item.status = false;
          return item;
        })
        this.calculateContract();
        break;
      }
      default: {}
    }
  }

  @action
  chooseOneRadioGroup(value) {
    switch(value) {
      case 'all': {
        this.oneArray.map(item => {
          item.status = true;
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'small': {
        this.oneArray.map(item => {
          if(item.index<5) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'large': {
        this.oneArray.map(item => {
          if(item.index>4) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'even': {
        this.oneArray.map(item => {
          if(item.index % 2 === 0) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'odd': {
        this.oneArray.map(item => {
          if(item.index % 2 !== 0) {
            item.status = true;
          } else {
            item.status = false;
          }
          return item;
        })
        this.calculateContract();
        break;
      }
      case 'clear': {
        this.oneArray.map(item => {
          item.status = false;
          return item;
        })
        this.calculateContract();
        break;
      }
      default: {}
    }
  }

  @computed get currentDay() {
    return CountdownCardStore.currentDay;
  }
  
  @computed get isBefore() {
    return CountdownCardStore.isBefore;
  }

  @computed get getCurrentDay() {
    return CountdownCardStore.currentDay;
  }

  @action
  fetchData(index = null) {
    let url;
    if (index === null) {
      const renderDay = moment().add(CountdownCardStore.isBefore? -2 : -1, 'days').format('DD-MM-YYYY');
      url = `${config.serverUrl}/result/by-date?date=${renderDay}`;
      this.date = renderDay;
      console.log('this.date: ', this.date);
    } else {
      const renderDay = moment().add(CountdownCardStore.isBefore? -index-1 : -index, 'days').format('DD-MM-YYYY');
      url = `${config.serverUrl}/result/by-date?date=${renderDay}`;
      this.date = renderDay;
      console.log('this.date: ', this.date);
    }
    const token = localStorage.getItem('token');
    axios.get(url, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(result => {
        const kq = result.data.result;
        const ar = [
          {
            title: 'Giải ĐB',
            data: []
          },
          {
            title: 'Giải nhất',
            data: []
          },
          {
            title: 'Giải nhì',
            data: []
          },
          {
            title: 'Giải ba',
            data: []
          },
          {
            title: 'Giải tư',
            data: []
          },
          {
            title: 'Giải năm',
            data: []
          },
          {
            title: 'Giải sáu',
            data: []
          },
          {
            title: 'Giải bẩy',
            data: []
          }
        ]
        kq.forEach(item => {
          ar[item.index].data.push(item.data)
        })
        this.data = ar;
        console.log('this.data: ', this.data);
      })
      .catch(err => {
        console.log('err: ', err);
      })
  }
}

const XsmbStore = new XsmbStoreInit();

export default XsmbStore;
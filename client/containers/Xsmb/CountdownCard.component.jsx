import React from 'react';
import { Card, Icon } from 'antd';
import { observer } from 'mobx-react';
import CountDownCardStore from './countdown-card.store';

@observer
export class CountdownCard extends React.Component {
	constructor(props) {
		super(props)
		this.store = CountDownCardStore;
	}
	render() {
		const { durationHours, durationMinutes, durationSeconds, currentDay } = this.store;
		return (
			<Card
				actions={[
					<span><Icon type="setting" key="setting" /> Nạp tiền</span>,
					<span><Icon type="edit" key="edit" /> Rút tiền</span>,
				]}
			>
				<Card.Meta
					id="time-card"
					title={`${durationHours}:${durationMinutes}:${durationSeconds}`}
					description={`Lượt xổ ngày ${currentDay}`}
				/>
			</Card>
		)
	}
}

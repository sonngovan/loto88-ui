import React from 'react';
import { Link } from 'react-router-dom';
import { observer, inject, observe } from 'mobx-react';
import { observable, action } from 'mobx';
import { Trans, withI18n } from '@lingui/react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import './login.less';

@inject(stores => stores)
@withI18n()
@Form.create()
@observer
class LoginForm extends React.Component {
  @observable submitted = false;

  @action setSubmit = (value) => this.submitted = value;

  constructor(props) {
    super(props);
    const { userStore } = this.props;
    userStore.setProperties({ loginError: {} });
  }

  handleSubmit = e => {
    this.setSubmit(true);
    e.preventDefault();
    const { login } = this.props.userStore;
    const { form: { validateFieldsAndScroll } } = this.props;
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        setTimeout(() => {
          login(values);
          this.setSubmit(false);
        }, 1000);
      }
    });
  };

  render() {
    const { i18n, form: { getFieldDecorator }, userStore: { loginError, handleChange } } = this.props;
    console.log('loginError: ', loginError);
    const { submitted } = this;
    return (
      <Form
        className="login-form"
      >
        {/* {loginError && <div className="error">{loginError}</div>} */}
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: i18n.t`Please input your email!` }],
          })(
            <Input
              handleChange={handleChange}
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder={i18n.t`Email`}
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: i18n.t`Please input your Password!` }],
          })(
            <Input
              handleChange={handleChange}
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder={i18n.t`Mật khẩu`}
              onPressEnter={this.handleSubmit}
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('remember', {
            valuePropName: 'checked',
          })(<Checkbox><Trans>Ghi nhớ đăng nhập</Trans></Checkbox>)}
          <a className="login-form-forgot" href="">
            <Trans><Link to="/user/reset-password">Quên mật khẩu?</Link></Trans>
          </a>
          <Button
            onClick={this.handleSubmit}
            type="primary"
            htmlType="submit"
            className="login-form-button"
            loading={submitted}
          >
            Đăng nhập
          </Button>
          <Trans>Chưa có tài khoản? <Link to="/user/register">Đăng ký ngay!</Link></Trans>
        </Form.Item>
      </Form>
    );
  }
}

export default LoginForm;
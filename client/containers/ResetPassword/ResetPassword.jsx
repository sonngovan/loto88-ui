import React from 'react';
import { observer } from 'mobx-react';
import 'antd/dist/antd.less';
import { Trans, withI18n } from '@lingui/react';
import {
	Form,
	Input,
	Icon,
	Button,
	Result,
	Row,
	Col,
} from 'antd';
import { Link } from 'react-router-dom';
import ForgotPasswordStore from './reset-password.store';
import './reset-password.less';


@withI18n()
@Form.create()
@observer
class ForgotPasswordForm extends React.Component {
	constructor(props) {
		super(props);
		this.store = ForgotPasswordStore;
	}
	
	handleReset = e => {
		const { resetPassword, setProperties } = this.store;
		const { validateFieldsAndScroll, setFieldsValue } = this.props.form;
		e.preventDefault();
		setProperties({ resetPasswordStatus: 'loading' });
		setTimeout(() => {
			validateFieldsAndScroll((err, values) => {
				if (!err) {
					const { code: token, password } = values;
					resetPassword(token, password);
					return setFieldsValue({
						email: '',
						code: null,
					});
				}
				return setProperties({ resetPasswordStatus: 'error' });
			});
		}, 1000);
	};

	handleSendCode = (e) => {
		const { sendResetPasswordCode } = this.store;
		const { validateFields } = this.props.form;
		e.preventDefault();
		validateFields(['email'], (err, values) => {
			if (!err) {
				const { email } = values;
				sendResetPasswordCode(email);
			}
		});
	}

	refreshForm = () => {
		this.store.setProperties({
			resetPasswordStatus: undefined,
			resetPasswordError: {},
			currentEmail: undefined,
		});
	}

	render() {
		const { i18n, form: { getFieldDecorator } } = this.props;
		const { resetPasswordStep, resetPasswordError, currentEmail, codeSent, resetPasswordStatus } = this.store;
		switch (resetPasswordStep) {
		case 'inputData': {
			return (
				<Form
					className="forgot-password"
				>
					{resetPasswordError && <div className={resetPasswordError.type}><Trans>{resetPasswordError.message}</Trans></div>}
					<Form.Item>
						<Row gutter={8}>
							<Col span={12}>
								{getFieldDecorator('email', {
									rules: [
										{
											type: 'email',
											message: <Trans>The input is not valid Email!</Trans>
										},
										{
											required: true,
											message: <Trans>Please input your Email!</Trans>
										},
									],
								})(
									<Input
										disabled={codeSent}
										prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
										placeholder={!codeSent ? i18n.t`Email` : currentEmail}
									/>
								)}
							</Col>
							<Col span={12}>
								<Button
									onClick={this.handleSendCode}
									disabled={codeSent}
								>
									{
										codeSent
											? (<Trans>Sent</Trans>)
											: (<Trans>Send code!</Trans>)
									}
									
								</Button>
							</Col>
						</Row>
					</Form.Item>
	
					<Form.Item>
						{getFieldDecorator('code', {
							rules: [
								{
									required: true,
									message: <Trans>Please input your verification code!</Trans>
								},
							],
						})(
							<Input
								prefix={<Icon type="number" style={{ color: 'rgba(0,0,0,.25)' }} />}
								placeholder={i18n.t`Verification code`}
							/>,
						)}
					</Form.Item>
									
					<Form.Item>
						{getFieldDecorator('password', {
							rules: [
								{
									required: true,
									message: <Trans>Reset your password</Trans>
								},
							],
						})(
							<Input
								prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
								placeholder={i18n.t`Reset your password!`}
								type="password"
							/>,
						)}
					</Form.Item>

					<Form.Item>
						<Button
							onClick={this.handleReset}
							type="primary"
							htmlType="submit"
							className="submit-form-button"
							loading={resetPasswordStatus === 'loading'}
						>
							<Trans>Reset</Trans>
						</Button>
						<Trans>Or <button className="sendcode-button" onClick={this.refreshForm}><Link to="/user/login" >login now!</Link></button></Trans>
					</Form.Item>
				</Form>
			);
		}

		case 'success': {
			return (
				<Result
					status="success"
					title="Your password has been reset!"
					subTitle="Please login for more infomation!"
					extra={[
						<Button type="primary" key="login">
							Login
						</Button>,
						<Button key="home">Home Page</Button>,
					]}
				/>
			);
		}
		}
	}
}

export default ForgotPasswordForm;
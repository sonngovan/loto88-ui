import { observable, action, computed, observe } from 'mobx';
import { userService } from '../../services/user.service';
import authenticationService from '../../services/authentication.service';
class ResetPasswordStoreInit {
    constructor() {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @observable resetPasswordStep = 'inputData';

    @observable resetPasswordStatus;

    @observable codeSent = false;

    @observable resetPasswordError;

    @observable currentEmail;

    @action sendResetPasswordCode = (email) => {
        this.authenticationService.create({
            action: 'sendResetPwd',
            value: { email },
        })
            .then(() => {
                this.setProperties({
                    currentEmail: email,
                    codeSent: true,
                });
            })
            .catch((resetPasswordError) => {
                this.setProperties({
                    resetPasswordError,
                    resetPasswordStatus: 'error',
                });
            })
    }

    @action resetPassword = (token, password) => {
        this.authenticationService.create({
            action: 'resetPwdShort',
            value: {
                user: { email: this.currentEmail },
                token,
                password,
            }
        })
            .then((data) => {
                this.setProperties({ resetPasswordStep: 'success' });
            })
            .catch((resetPasswordError) => {
                this.setProperties({ resetPasswordError });
            })
    }

    @action setProperties = (newValue) => {
        Object.assign(this, newValue)
    }
}

const ResetPasswordStore = new ResetPasswordStoreInit();

export default ResetPasswordStore;
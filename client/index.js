import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'mobx-react';
import App from './App';
import {
	UserStore,
	GlobalStore,
} from './stores';

const appStore = {
	userStore: UserStore,
	globalStore: GlobalStore,
};

render(<Provider {...appStore}>
		<App />
	</Provider>
	, document.getElementById('wrapper'));

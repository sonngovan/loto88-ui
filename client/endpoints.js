import {
	Login,
	Register,
	HomePage,
	ResetPassword,
	CoinPage,
	UserInfo
	// ExplorerPage,
	// EventPage,
} from './containers';

export const login = {
	path: '/user/login',
	tittle: 'Login',
	icon: 'fas fa-sign-in-alt',
	component: Login,
	accessRoles: ['non-user'],
	redirectPath: '/user/profile',
	secondaryPath: '/',
};

export const register = {
	path: '/user/register',
	tittle: 'Register',
	icon: 'user-add',
	component: Register,
	accessRoles: ['non-user'],
	redirectPath: '/404',
	secondaryPath: '/user/login',
};

export const resetPassword = {
	path: '/user/reset-password',
	tittle: 'Reset Password',
	icon: 'user-add',
	component: ResetPassword,
	accessRoles: ['non-user'],
	redirectPath: '/404',
	secondaryPath: '/user/login',
};

export const home = {
	path: '/',
	tittle: 'Home Page',
	icon: 'home',
	component: HomePage,
	accessRoles: ['non-user', 'user', 'coin-owner', 'admin'],
	redirectPath: '',
	secondaryPath: '',
};

FROM node:10-alpine
WORKDIR /usr/app
COPY package*.json /usr/app/
RUN npm install
COPY . .
RUN npm run bundle